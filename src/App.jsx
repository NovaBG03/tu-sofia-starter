import "./App.css";
import { Navbar } from "./components/Navbar.jsx";
import { ListSection } from "./components/ListSection.jsx";
import { DetailsSection } from "./components/DetailsSection.jsx";
import { useMemo, useState } from "react";
import { Footer } from "./components/Footer.jsx";

function App() {
  const [books, setBooks] = useState([
    {
      id: 1,
      title: "Title 1",
      author: "Author 1",
      isbn: "ISBN 1",
      price: 100,
      publicationDate: new Date("2021-01-01"),
    },
    {
      id: 2,
      title: "Title 2",
      author: "Author 2",
      isbn: "ISBN 2",
      price: 100,
      publicationDate: new Date("2021-01-01"),
    },
  ]);

  const [selectedBookId, setSelectedBookId] = useState(null);
  const selectedBook = useMemo(
    () => books.find((x) => x.id === selectedBookId),
    [books, selectedBookId],
  );

  return (
    <main>
      <Navbar />
      <p>{selectedBook?.title}</p>
      <ListSection
        books={books}
        onDelete={(id) => {
          setSelectedBookId((state) => {
            if (state === id) {
              return null;
            }
            if (state > id) {
              return state - 1;
            }
            return state;
          });

          setBooks((state) =>
            state
              .filter((x) => x.id !== id)
              .map((b) => ({ ...b, id: b.id > id ? b.id - 1 : b.id })),
          );
        }}
        onSelect={(book) => {
          console.log("on select", book.id);
          setSelectedBookId(book.id);
        }}
      />
      <DetailsSection
        selectedBook={selectedBook}
        onBookCreated={(book) => {
          if (selectedBook) {
            setBooks((state) =>
              state.map((x) =>
                x.id === selectedBook.id ? { ...x, ...book } : x,
              ),
            );
            setSelectedBookId(null);
            return;
          }

          setBooks((state) => [...state, { id: state.length + 1, ...book }]);
        }}
      />
      <Footer />
    </main>
  );
}

export default App;
