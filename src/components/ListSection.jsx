import { useMemo } from "react";

export const ListSection = ({ books = [], onDelete, onSelect }) => {
  const sortedBooks = useMemo(() => books.sort((a, b) => a.id - b.id), [books]);

  return (
    <div className="content-list">
      <h2>Content list</h2>
      <ul>
        {sortedBooks.map((book) => (
          <li key={book.id}>
            <div onClick={() => onSelect?.(book)}>
              <p className="id">{book.id}</p>
              <p className="field1">{book.title}</p>
              <p className="field2">{book.author}</p>
              <p className="field3">{book.isbn}</p>
              <p className="field4">{book.price}</p>
              <p className="field5">{book.publicationDate.toString()}</p>
            </div>
            <button
              className="deleteButton"
              onClick={() => onDelete?.(book.id)}
            >
              Delete
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};
